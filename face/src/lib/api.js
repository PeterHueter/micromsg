const apiUrl = 'http://localhost:3000'

export default {
    users: {
        me: apiUrl + '/users/me',
        byId: (id) =>  apiUrl + '/users/' + id,
        messages: (id) => apiUrl + '/users/' + id + '/messages'
    },
    messages: {
        index: apiUrl + '/messages',
        store: apiUrl + '/messages',
        comments: (id) => apiUrl + '/messages/' + id + '/comments',
        storeComment: (id) => apiUrl + '/messages/' + id + '/comments',
    },
    auth: {
        login: apiUrl + '/auth/login',
        check: apiUrl + '/auth/check'
    }
}