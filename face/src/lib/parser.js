export default new class {
  parse(text, linkFunc) {
    return text.replace(/#([A-Za-zА-Яа-я0-9_]+)/g, (match, p1) => linkFunc(p1))
  }
}
