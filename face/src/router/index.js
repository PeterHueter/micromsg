import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Messages from '@/components/Messages'
import axios from 'axios'
import api from '@/lib/api'
import Profile from '@/components/Profile'

Vue.use(Router)

const mustAuth = (to, from, next) => {
  axios
    .get(api.users.me)
    .then(r => {
      next()
    })
    .catch(r => {
      next('/login')
    })
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Messages,
      beforeEnter: mustAuth
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },    
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: mustAuth
    }
  ]
})
