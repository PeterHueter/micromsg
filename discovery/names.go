package discovery

const (
	// AUTHD service name
	AUTHD = "authd"

	// MESSAGED service name
	MESSAGED = "messaged"

	// GATEWAYD service name
	GATEWAYD = "gatewayd"

	// USERD service name
	USERD = "userd"

	// TAGD service name
	TAGD = "tagd"
)
