package discovery

import (
	"bitbucket.org/PeterHueter/micromsg/discovery/consul"
	"log"
	"os"
)

const defaultAddr = "localhost:8500"

var defaultClient Client

// default consul client
func init() {
	var err error

	addr := os.Getenv("CONSUL_ADDR")
	if addr == "" {
		addr = defaultAddr
	}

	defaultClient, err = consul.NewClient(addr)
	if err != nil {
		log.Fatal("can not connect to consul agent:", err)
	}
}

// Service list all registered services of specified type
func Service(name string) ([]string, error) {
	return defaultClient.Service(name)
}

// Register service
func Register(id, name string, port int) error {
	return defaultClient.Register(id, name, port)
}

// DeRegister service
func DeRegister(id string) error {
	return defaultClient.DeRegister(id)
}

// Refresh is report service health
func Refresh(id string) error {
	return defaultClient.Refresh(id)
}
