package discovery

import (
	"github.com/satori/go.uuid"
)

// GenerateID generate new unique id
func GenerateID() string {
	return uuid.NewV4().String()
}
