package discovery

// Client is service discovery abstraction
type Client interface {
	// Get registered service from repote
	Service(name string) ([]string, error)

	// Register service
	Register(ID, name string, port int) error

	// Deregister service
	DeRegister(id string) error

	// Refresh is used to report alive status
	Refresh(id string) error
}
