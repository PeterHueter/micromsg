package discovery

func Authd() string {
	return ":51000"
}

func Messaged() string {
	return ":50000"
}

func Gatewayd() string {
	return ":3000"
}

func Userd() string {
	return ":44448"
}

func Tagd() string {
	return ":43534"
}
