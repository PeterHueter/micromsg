package consul

import (
	"fmt"
	consul "github.com/hashicorp/consul/api"
)

const (
	ttl             = "15s"
	deregisterAfter = "10s"
)

// Client consul local agent api
type Client struct {
	consul *consul.Client
}

// NewClient creates new consul local agent
func NewClient(addr string) (*Client, error) {
	config := consul.DefaultConfig()
	config.Address = addr
	c, err := consul.NewClient(config)
	if err != nil {
		return nil, err
	}
	return &Client{consul: c}, nil
}

// Service return all alive services of specified type
func (c *Client) Service(name string) ([]string, error) {
	passingOnly := true
	nodes, _, err := c.consul.Health().Service(name, "", passingOnly, nil)
	if len(nodes) == 0 && err == nil {
		return nil, fmt.Errorf("service ( %s ) was not found", name)
	}
	if err != nil {
		return nil, err
	}
	addrs := make([]string, 0, len(nodes))
	for _, node := range nodes {
		addrs = append(addrs, fmt.Sprintf("%s:%d", node.Node.Address, node.Service.Port))
	}
	return addrs, nil
}

// Register service to local agent
func (c *Client) Register(ID, name string, port int) error {
	check := &consul.AgentServiceCheck{
		CheckID: ID,
		TTL:     ttl,
		DeregisterCriticalServiceAfter: deregisterAfter,
	}

	reg := &consul.AgentServiceRegistration{
		ID:    ID,
		Name:  name,
		Port:  port,
		Check: check,
	}
	return c.consul.Agent().ServiceRegister(reg)
}

// DeRegister a service with consul local agent
func (c *Client) DeRegister(id string) error {
	return c.consul.Agent().ServiceDeregister(id)
}

// Refresh is reporting local agent that service is alive
func (c *Client) Refresh(id string) error {
	return c.consul.Agent().UpdateTTL(id, "", "pass")
}
