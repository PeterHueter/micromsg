package common

import (
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"encoding/json"
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"golang.org/x/net/context"
	"google.golang.org/grpc/metadata"
	"log"
	"net/http"
	"strings"
)

type contextKey string

const tokenKey contextKey = "token"
const userKey = "user"

// UserClaims is structure with commmon jwt data
type UserClaims struct {
	jwt.StandardClaims
	User *rpc.User `json:"user"`
}

// GetTokenFromRequest extracting token from request header
// returns token, type, error
func GetTokenFromRequest(r *http.Request) (token string, typ string, err error) {
	// extract user from jwt
	h := r.Header.Get("Authorization")
	parts := strings.Split(h, " ")
	switch len(parts) {
	case 0:
		err = errors.New("Emprty authorization header")
	case 1:
		token = parts[0]
	default:
		token, typ = parts[1], parts[0]
	}
	return token, typ, err
}

func GetUserFromToken(token string) (*rpc.User, error) {
	parts := strings.Split(token, ".")

	if len(parts) != 3 {
		return nil, errors.New("token contains an invalid number of segments")
	}

	claims := &UserClaims{}

	data, err := jwt.DecodeSegment(parts[1])
	if err != nil {
		return nil, errors.New("decode token: " + err.Error())
	}

	err = json.Unmarshal(data, claims)
	if err != nil {
		return nil, errors.New("token parse: " + err.Error())
	}
	return claims.User, nil
}

func SetUserToContext(ctx context.Context, user *rpc.User) context.Context {
	data, err := json.Marshal(user)
	if err != nil {
		log.Println("set user to content:", err)
		return ctx
	}
	md := metadata.Pairs(userKey, string(data))
	return metadata.NewOutgoingContext(ctx, md)
}

func GetUserFromContext(ctx context.Context) (*rpc.User, error) {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, errors.New("context doesn't have grpc meta")
	}

	data, ok := md[userKey]
	if !ok {
		return nil, errors.New("metadata doesn't have user")
	}

	if len(data) == 0 {
		return nil, errors.New("context user meta is empty")
	}

	bytes := []byte(data[0])
	user := &rpc.User{}
	err := json.Unmarshal(bytes, user)
	if err != nil {
		return nil, errors.New("unmarshal user meta header: " + err.Error())
	}

	return user, nil
}
