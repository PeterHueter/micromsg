package common

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"net/http"
)

var grpcToHTTP = map[codes.Code]int{
	codes.NotFound:        http.StatusNotFound,
	codes.Unauthenticated: http.StatusUnauthorized,
}

func HandleGrpcError(err error) (string, int) {
	if err == nil {
		panic("Can't handle nil erorrs")
	}

	s, ok := status.FromError(err)
	msg := err.Error()
	scode := http.StatusInternalServerError
	if ok {
		msg = s.Message()
		scode, ok = grpcToHTTP[s.Code()]
		if !ok {
			scode = http.StatusInternalServerError
		}
	}

	return msg, scode
}
