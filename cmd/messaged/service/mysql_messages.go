package service

import (
	"bitbucket.org/PeterHueter/micromsg/rpc"
	_ "github.com/go-sql-driver/mysql"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	"github.com/jmoiron/sqlx"
	"log"
)

type mysqlMessage struct {
	ID       uint32 `db:"id"`
	UserID   uint32 `db:"user_id"`
	ParentID uint32 `db:"parent_id"`
	Date     uint64 `db:"date"`
	Body     string `db:"body"`
	Comments uint32 `db:"comments"`
}

type MysqlMessages struct {
	db *sqlx.DB
}

func NewMysqlMessages(dsn string) (*MysqlMessages, error) {
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}
	return &MysqlMessages{db: db}, nil
}

func mysqlToRpc(messages []mysqlMessage) []*rpc.Message {
	result := make([]*rpc.Message, 0, len(messages))
	for _, msg := range messages {
		result = append(result, &rpc.Message{
			ID:       msg.ID,
			UserID:   msg.UserID,
			ParentID: msg.ParentID,
			Date:     &timestamp.Timestamp{Seconds: int64(msg.Date)},
			Body:     msg.Body,
			Comments: msg.Comments,
		})
	}
	return result
}

func (messages *MysqlMessages) ByUserID(userID uint32) []*rpc.Message {
	msgs := []mysqlMessage{}
	err := messages.db.Select(&msgs, "SELECT * FROM messages WHERE parent_id = 0 and user_id = ?", userID)
	if err != nil {
		log.Println("get messages by user id:", err)
		return []*rpc.Message{}
	}
	return mysqlToRpc(msgs)
}

func (messages *MysqlMessages) MessageComments(messageID uint32) []*rpc.Message {
	msgs := []mysqlMessage{}
	err := messages.db.Select(&msgs, "SELECT * FROM messages WHERE parent_id = ?", messageID)
	if err != nil {
		log.Println("get message comments:", err)
		return []*rpc.Message{}
	}

	return mysqlToRpc(msgs)
}

func (messages *MysqlMessages) Add(m *rpc.Message) uint32 {
	res, err := messages.db.Exec(`insert into messages (user_id, date, body, parent_id) values (?,?,?,?)`,
		m.UserID, m.Date.Seconds, m.Body, m.ParentID)
	if err != nil {
		log.Println("add message:", err)
		return 0
	}

	if m.ParentID != 0 {
		err = messages.incrComments(m.ParentID, 1)
		if err != nil {
			log.Println("increment comment count:", err)
		}
	}

	id, err := res.LastInsertId()
	if err != nil {
		log.Println("get last insert id:", err)
	}

	return uint32(id)
}

func (messages *MysqlMessages) incrComments(messageID, count uint32) error {
	_, err := messages.db.Exec(`UPDATE messages SET comments = comments + ? WHERE id = ?`,
		count, messageID)
	if err != nil {
		return err
	}
	return nil
}
