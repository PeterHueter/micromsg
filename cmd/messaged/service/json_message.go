package service

import (
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"encoding/json"
	"io/ioutil"
	"sync"
)

// Messages is message repository
type JosnMessages struct {
	sync.RWMutex
	Mesages  []*rpc.Message
	FileName string
}

// filterFunc is filtering epo JosnMessages by criteria
type filterFunc func(*rpc.Message) bool

// Load restore from file
func (repo *JosnMessages) Load() error {
	data, err := ioutil.ReadFile(repo.FileName)
	if err != nil {
		return err
	}

	repo.Lock()
	err = json.Unmarshal(data, &repo.Mesages)
	repo.Unlock()
	if err != nil {
		return err
	}
	return nil
}

func (repo *JosnMessages) Store() error {
	repo.RLock()
	data, err := json.Marshal(repo.Mesages)
	repo.RUnlock()
	if err != nil {
		return err
	}
	return ioutil.WriteFile(repo.FileName, data, 0644)
}

// FilterFirst is filtering all messages and return first match
// all criteria must match for pass filter
func (repo *JosnMessages) FilterFirst(filters ...filterFunc) (message *rpc.Message, ok bool) {
	repo.RLock()
	for _, msg := range repo.Mesages {
		passed := false
		for _, f := range filters {
			passed = f(msg)
			if !passed {
				break
			}
		}
		if passed {
			message = msg
			ok = true
			break
		}
	}
	repo.RUnlock()
	return message, ok
}

// Filter is filtering all messages by criteria
// all criteria must match for pass filter
func (repo *JosnMessages) Filter(filters ...filterFunc) []*rpc.Message {
	result := make([]*rpc.Message, 0, 10)
	repo.RLock()
	for _, msg := range repo.Mesages {

		// if filter not match break loop throu filters
		passed := false
		for _, f := range filters {
			passed = f(msg)
			if !passed {
				break
			}
		}

		// if all filters match add to result
		if passed {
			result = append(result, msg)
		}
	}
	repo.RUnlock()
	return result
}

func byUser(userID uint32) filterFunc {
	return func(m *rpc.Message) bool {
		return m.UserID == userID
	}
}

func noComments(m *rpc.Message) bool {
	return m.ParentID == 0
}

func commentsFor(messageID uint32) filterFunc {
	return func(m *rpc.Message) bool {
		return m.ParentID == messageID
	}
}

// ByUserID filter messages by userid
func (repo *JosnMessages) ByUserID(userID uint32) []*rpc.Message {
	return repo.Filter(byUser(userID), noComments)
}

// MessageComments return comments for specified message
func (repo *JosnMessages) MessageComments(messageID uint32) []*rpc.Message {
	return repo.Filter(commentsFor(messageID))
}

// Add add model to repository
// autoincrement message id
func (repo *JosnMessages) Add(m *rpc.Message) uint32 {
	repo.Lock()
	messageID := uint32(len(repo.Mesages) + 1)
	m.ID = messageID
	repo.Mesages = append(repo.Mesages, m)
	if m.ParentID != 0 {
		repo.commentCounter(m.ParentID, 1)
	}
	repo.Unlock()

	repo.Store()
	return messageID
}

// commentCounter incrase recerd abount number of comments for specified message
// unsafe operation
func (repo *JosnMessages) commentCounter(messageID uint32, inc uint32) {
	for _, m := range repo.Mesages {
		if m.ID == messageID {
			m.Comments += inc
			break
		}
	}
}

// RecalculateComments is recalculating number of comments for each message
// thi sfunction rarely needed
func (repo *JosnMessages) RecalculateComments() {
	// clear comment count
	for _, m := range repo.Mesages {
		m.Comments = 0
	}

	// calculations
	for _, m := range repo.Mesages {
		if m.ParentID != 0 {
			repo.commentCounter(m.ParentID, 1)
		}
	}
}
