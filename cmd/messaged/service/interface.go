package service

import (
	"bitbucket.org/PeterHueter/micromsg/rpc"
)

type Messages interface {
	ByUserID(userID uint32) []*rpc.Message
	MessageComments(messageID uint32) []*rpc.Message
	Add(m *rpc.Message) uint32
}
