package service

import (
	pb "bitbucket.org/PeterHueter/micromsg/cmd/messaged/service/proto"
	tagd "bitbucket.org/PeterHueter/micromsg/cmd/tagd/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"github.com/srinathh/hashtag"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
)

// Service implements message service rpc
type Service struct {
	Messages Messages
}

func (svc *Service) ByUserID(ctx context.Context, in *pb.ByUserIDRequest) (*pb.MessagesResponse, error) {
	messages := svc.Messages.ByUserID(in.GetID())
	return &pb.MessagesResponse{Messages: messages}, nil
}

func (svc *Service) Post(ctx context.Context, in *rpc.Message) (*rpc.Empty, error) {
	ID := svc.Messages.Add(in)

	// hashtags
	tags := hashtag.ExtractHashtags(in.Body)
	sendHashtags(ctx, ID, tags)

	return &rpc.Empty{}, nil
}

// GetComments return all message comments
func (svc *Service) GetComments(ctx context.Context, in *pb.GetCommentsRequest) (*pb.MessagesResponse, error) {
	resp := &pb.MessagesResponse{
		Messages: svc.Messages.MessageComments(in.MessageID),
	}
	return resp, nil
}

func sendHashtags(ctx context.Context, messageID uint32, tags []string) {
	tagdAddr := discovery.Tagd()
	conn, err := grpc.Dial(tagdAddr, grpc.WithInsecure())
	if err != nil {
		log.Println("connect to tagd service:", err)
		return
	}
	defer conn.Close()
	client := tagd.NewTagdClient(conn)

	pbTags := make([]*rpc.Tag, 0, len(tags))
	for _, tag := range tags {
		pbTags = append(pbTags, &rpc.Tag{
			Text: tag,
		})
	}

	in := &tagd.MessageTags{
		MessageId: messageID,
		Tags:      pbTags,
	}
	_, err = client.AddMessageTags(ctx, in)
	if err != nil {
		log.Println("assign tags:", err)
	}
}
