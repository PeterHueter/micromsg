package main

import (
	"bitbucket.org/PeterHueter/micromsg/cmd/messaged/service"
	pb "bitbucket.org/PeterHueter/micromsg/cmd/messaged/service/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

const fileName = "messages.json"
const port = 50000

var serviceID string

func healthReport() {
	err := discovery.Register(serviceID, discovery.MESSAGED, port)
	if err != nil {
		log.Fatal("health report: ", err)
	}

	for {
		err = discovery.Refresh(serviceID)
		if err != nil {
			log.Println("health report: ", err)
		}
		time.Sleep(time.Second * 10)
	}
}

func main() {
	serviceID = discovery.GenerateID()
	log.Println("Service ID:", serviceID)

	/*
		messages := &service.Messages{
			FileName: fileName,
		}
		err := messages.Load()
		if err != nil {
			log.Fatal("Can't load messages:", err)
		}
	*/
	messages, err := service.NewMysqlMessages("micromsg:micromsg@(localhost)/micromsg")
	if err != nil {
		log.Fatal("connect to message database:", err)
	}

	MessageAddr := discovery.Messaged()
	sock, err := net.Listen("tcp", MessageAddr)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen socket:", MessageAddr)

	log.Println("Background health report")
	go healthReport()
	s := grpc.NewServer()
	pb.RegisterMessagesServer(s, &service.Service{Messages: messages})
	log.Println("Runing gRPC service")
	err = s.Serve(sock)
	if err != nil {
		log.Fatal(err)
	}

}
