package main

import (
	"log"
	"net"

	pb "bitbucket.org/PeterHueter/micromsg/cmd/tagd/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"google.golang.org/grpc"
	"time"
)

const fileName = "tags.json"

const port = 43534

var serviceID string

func healthReport() {
	err := discovery.Register(serviceID, discovery.TAGD, port)
	if err != nil {
		log.Fatal("health report: ", err)
	}

	for {
		err = discovery.Refresh(serviceID)
		if err != nil {
			log.Println("health report: ", err)
		}
		time.Sleep(time.Second * 10)
	}
}

func main() {
	serviceID = discovery.GenerateID()
	log.Println("Service ID:", serviceID)

	/*
		repo := &tags{db: fileName}
		err := repo.Load()
		if err != nil {
			log.Fatal("Load tags:", err)
		}
	*/
	repo, err := newMysqlTags("micromsg:micromsg@(localhost)/micromsg")
	if err != nil {
		log.Fatal("init tags repo:", err)
	}

	MessageAddr := discovery.Tagd()
	sock, err := net.Listen("tcp", MessageAddr)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen socket:", MessageAddr)

	log.Println("Background health report")
	go healthReport()

	s := grpc.NewServer()
	pb.RegisterTagdServer(s, &tagd{tags: repo})
	log.Println("Runing gRPC service")
	err = s.Serve(sock)
	if err != nil {
		log.Fatal(err)
	}
}
