package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
)

type mysqlTags struct {
	db *sqlx.DB
}

func newMysqlTags(dsn string) (*mysqlTags, error) {
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}
	return &mysqlTags{db: db}, nil
}

func (t *mysqlTags) Add(MessageID uint32, tags []string) {
	var err error
	for _, tag := range tags {
		_, err = t.db.Exec(`INSERT into tags (tag, message_id) VALUES (?,?)`, tag, MessageID)
		if err != nil {
			log.Println("add to for message:", err)
		}
	}
}
func (t *mysqlTags) GetByTag(tag string) []uint32 {
	msgs := []uint32{}
	err := t.db.Select(&msgs, `SELECT message_id FROM tags WHERE tag = ?`, tag)
	if err != nil {
		log.Println("select messages by tag:", err)
		return []uint32{}
	}
	return msgs
}
