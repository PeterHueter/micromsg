package main

import (
	"bitbucket.org/PeterHueter/micromsg/cmd/tagd/proto"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"log"
	"strings"
)

type tagd struct {
	tags Tags
}

func (d *tagd) AddMessageTags(ctx context.Context, in *proto.MessageTags) (*rpc.Empty, error) {
	tags := make([]string, 0, len(in.Tags))
	for _, tag := range in.Tags {
		tags = append(tags, tag.Text)
	}
	d.tags.Add(in.MessageId, tags)
	log.Println("tags added:", strings.Join(tags, ", "))

	return &rpc.Empty{}, nil
}
func (d *tagd) GetMessagesByTag(ctx context.Context, in *rpc.Tag) (*proto.MessagesResponse, error) {
	ids := d.tags.GetByTag(in.Text)
	return &proto.MessagesResponse{Messages: ids}, nil
}
func (d *tagd) TopTags(ctx context.Context, in *rpc.Empty) (*proto.Tags, error) {
	return nil, grpc.Errorf(codes.Unimplemented, "Top tags")
}
