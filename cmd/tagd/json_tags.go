package main

import (
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"sync"
	"syscall"
)

type messageTag struct {
	MessageID uint32   `json:"message_id,omitempty"`
	Tag       *rpc.Tag `json:"tag,omitempty"`
}

type jsonTags struct {
	m    sync.RWMutex
	data map[string][]uint32
	db   string
}

// Add add message ot repo
func (repo *jsonTags) Add(MessageID uint32, tags []string) {
	repo.m.Lock()
	for _, tag := range tags {
		ids, ok := repo.data[tag]
		if !ok {
			ids = make([]uint32, 0, 10)
		}
		ids = append(ids, MessageID)
		repo.data[tag] = ids
	}
	repo.m.Unlock()

	err := repo.Save()
	if err != nil {
		log.Println("save tags:", err)
	}
}

func (repo *jsonTags) GetByTag(tag string) []uint32 {
	repo.m.RLock()
	result, ok := repo.data[tag]
	repo.m.RUnlock()
	if !ok {
		result = []uint32{}
	}
	return result
}

func (repo *jsonTags) Load() error {
	data, err := ioutil.ReadFile(repo.db)
	if err != nil {
		return err
	}
	repo.m.Lock()
	repo.data = make(map[string][]uint32, 10)
	err = json.Unmarshal(data, &repo.data)
	repo.m.Unlock()
	return err
}

func (repo *jsonTags) Save() error {
	f, err := os.OpenFile(repo.db, syscall.O_CREAT|syscall.O_RDWR, 0700)
	if err != nil {
		return err
	}
	enc := json.NewEncoder(f)

	repo.m.RLock()
	err = enc.Encode(repo.data)
	repo.m.RUnlock()
	f.Close()

	return err
}
