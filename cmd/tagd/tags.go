package main

type Tags interface {
	Add(MessageID uint32, tags []string)
	GetByTag(tag string) []uint32
}
