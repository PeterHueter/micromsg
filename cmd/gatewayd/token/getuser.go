package token

import (
	"bitbucket.org/PeterHueter/micromsg/common"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"net/http"
)

// GetUser extract user information from request token
func GetUser(r *http.Request) (*rpc.User, error) {
	token, _, err := common.GetTokenFromRequest(r)
	if err != nil {
		return nil, err
	}

	return common.GetUserFromToken(token)
}
