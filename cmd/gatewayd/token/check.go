package token

import (
	authd "bitbucket.org/PeterHueter/micromsg/cmd/authd/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

// ValidToken is validating token making request to authd service
func Valid(token string) (bool, error) {
	authdAddr := discovery.Authd()
	conn, err := grpc.Dial(authdAddr, grpc.WithInsecure())
	if err != nil {
		return false, err
	}
	defer conn.Close()
	client := authd.NewAuthClient(conn)

	// make request and process errors
	req := &authd.Token{Token: token}
	resp, err := client.Check(context.Background(), req)
	if err != nil {
		return false, err
	}

	if !resp.GetValid() {
		return false, nil
	}

	return true, nil
}
