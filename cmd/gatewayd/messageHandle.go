package main

import (
	tkn "bitbucket.org/PeterHueter/micromsg/cmd/gatewayd/token"
	messaged "bitbucket.org/PeterHueter/micromsg/cmd/messaged/service/proto"
	userd "bitbucket.org/PeterHueter/micromsg/cmd/userd/service/proto"
	"bitbucket.org/PeterHueter/micromsg/common"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"encoding/json"
	"github.com/go-chi/chi"
	"github.com/golang/protobuf/ptypes/timestamp"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"strconv"
	"time"
)

type messagesWithUsers struct {
	Messages []*rpc.Message       `json:"messages"`
	Users    map[uint32]*rpc.User `json:"users"`
}

type closeFunc func() error

func messagedClient() (messaged.MessagesClient, closeFunc, error) {
	messagedAddr := discovery.Messaged()
	conn, err := grpc.Dial(messagedAddr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, err
	}

	client := messaged.NewMessagesClient(conn)
	return client, conn.Close, nil
}

func userdClient() (userd.UsersClient, closeFunc, error) {
	userdAddr := discovery.Userd()
	conn, err := grpc.Dial(userdAddr, grpc.WithInsecure())
	if err != nil {
		return nil, nil, err
	}

	client := userd.NewUsersClient(conn)
	return client, conn.Close, nil
}

func getCurrentUserMessages(w http.ResponseWriter, r *http.Request) {
	// request client
	client, close, err := messagedClient()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer close()

	// current user id
	user, err := tkn.GetUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	in := &messaged.ByUserIDRequest{
		ID: user.ID,
	}
	resp, err := client.ByUserID(r.Context(), in)
	if err != nil {
		http.Error(w, "current user messages: "+err.Error(), http.StatusInternalServerError)
		return
	}

	// json encode result
	msg := &messagesWithUsers{
		Messages: resp.Messages,
		Users: map[uint32]*rpc.User{
			user.ID: user,
		},
	}

	data, err := json.Marshal(msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("getCurrentUserMessages send response:", err)
	}
}

func storeCurrentUserMessage(w http.ResponseWriter, r *http.Request) {
	// request client
	client, close, err := messagedClient()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer close()

	// current user id
	user, err := tkn.GetUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// make request and process errors
	in := &rpc.Message{
		UserID:   user.ID,
		ParentID: 0,
		Date:     &timestamp.Timestamp{Seconds: time.Now().Unix()},
		Body:     r.FormValue("text"),
	}

	_, err = client.Post(r.Context(), in)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}
	w.WriteHeader(http.StatusOK)
}

func getMessageComments(w http.ResponseWriter, r *http.Request) {
	// request client
	client, close, err := messagedClient()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer close()

	// make request and process errors
	messageID, err := strconv.ParseUint(chi.URLParam(r, "messageID"), 10, 32)
	if err != nil {
		http.Error(w, "messageID should be valid uint", http.StatusBadRequest)
		return
	}
	in := &messaged.GetCommentsRequest{
		MessageID: uint32(messageID),
	}
	resp, err := client.GetComments(r.Context(), in)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	// get unique users from comments
	idMap := make(map[uint32]rpc.Empty, 10)
	for _, c := range resp.Messages {
		idMap[c.UserID] = rpc.Empty{}
	}
	ids := make([]uint32, 0, len(idMap))
	for id := range idMap {
		ids = append(ids, id)
	}

	// get users profiles
	uclient, close, err := userdClient()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer close()
	uin := &userd.GetRequest{
		ID: ids,
	}
	usersResp, err := uclient.Get(r.Context(), uin)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	users := make(map[uint32]*rpc.User, len(usersResp.Users))
	for _, user := range usersResp.Users {
		users[user.ID] = user
	}

	// json encode result
	msg := &messagesWithUsers{
		Messages: resp.Messages,
		Users:    users,
	}
	data, err := json.Marshal(msg)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("getMessageComments send response:", err)
	}
}

func storeMessageComment(w http.ResponseWriter, r *http.Request) {
	// request client
	client, close, err := messagedClient()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer close()

	// current user id
	user, err := tkn.GetUser(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// parent message id
	messageID, err := strconv.ParseUint(chi.URLParam(r, "messageID"), 10, 32)
	if err != nil {
		http.Error(w, "messageID should be valid uint", http.StatusBadRequest)
		return
	}

	// make request and process errors
	in := &rpc.Message{
		UserID:   user.ID,
		ParentID: uint32(messageID),
		Date:     &timestamp.Timestamp{Seconds: time.Now().Unix()},
		Body:     r.FormValue("text"),
	}

	_, err = client.Post(r.Context(), in)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}
	w.WriteHeader(http.StatusOK)
}
