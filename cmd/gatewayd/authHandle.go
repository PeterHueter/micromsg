package main

import (
	authd "bitbucket.org/PeterHueter/micromsg/cmd/authd/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"encoding/json"
	"log"
	//"encoding/json"
	//"github.com/go-chi/chi"
	"google.golang.org/grpc"
	"net/http"
	//"strconv"
	tkn "bitbucket.org/PeterHueter/micromsg/cmd/gatewayd/token"
	"bitbucket.org/PeterHueter/micromsg/common"
)

type tokenResponse struct {
	Token    string `json:"token,omitempty"`
	ExpireAt int64  `json:"expire_at,omitempty"`
}

type checkResponse struct {
	Success bool `json:"success,omitempty"`
}

func authLogin(w http.ResponseWriter, r *http.Request) {
	// connection to authd service
	authdAddr := discovery.Authd()
	log.Println("Connecting to authd:", authdAddr)
	conn, err := grpc.Dial(authdAddr, grpc.WithInsecure())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()
	client := authd.NewAuthClient(conn)

	// make request and process errors
	credentails := &authd.Credentails{
		Login:    r.FormValue("login"),
		Password: r.FormValue("password"),
	}
	resp, err := client.Login(r.Context(), credentails)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	// response to user
	jsonResp := &tokenResponse{
		Token: resp.GetToken(),
	}
	data, err := json.Marshal(jsonResp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("authLogin send response:", err)
	}

}

func authCheck(w http.ResponseWriter, r *http.Request) {
	// connection to authd service
	token := r.FormValue("token")
	ok, err := tkn.Valid(token)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	// response to user
	chrsp := &checkResponse{
		Success: ok,
	}
	data, err := json.Marshal(chrsp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("authCheck send response:", err)
	}
}
