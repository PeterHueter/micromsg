package main

import (
	"bitbucket.org/PeterHueter/micromsg/cmd/gatewayd/middleware"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
	"log"
	"net/http"
	"time"
)

const port = 3000

var serviceID string

func healthReport() {
	err := discovery.Register(serviceID, discovery.GATEWAYD, port)
	if err != nil {
		log.Fatal("health report: ", err)
	}

	for {
		err = discovery.Refresh(serviceID)
		if err != nil {
			log.Println("health report: ", err)
		}
		time.Sleep(time.Second * 10)
	}
}

func main() {
	serviceID = discovery.GenerateID()
	log.Println("Service ID:", serviceID)

	r := chi.NewRouter()

	// enable cors
	corsMiddleware(r)

	r.Route("/users", func(r chi.Router) {
		r.Use(middleware.JwtAuth, middleware.ContextUser)
		r.Get("/me", currentUser)
		r.Route("/{userId}", func(r chi.Router) {
			r.Get("/", getUser)
			r.Get("/messages", getUserMessages)
		})
	})

	r.Route("/messages", func(r chi.Router) {
		r.Use(middleware.JwtAuth, middleware.ContextUser)
		r.Get("/", getCurrentUserMessages)
		r.Post("/", storeCurrentUserMessage)
		r.Route("/{messageID}", func(r chi.Router) {
			r.Get("/comments", getMessageComments)
			r.Post("/comments", storeMessageComment)
		})
	})

	r.Route("/auth", func(r chi.Router) {
		r.Post("/login", authLogin)
		r.Get("/check", authCheck)
	})

	log.Println("Background health report")
	go healthReport()

	addr := discovery.Gatewayd()
	log.Println("Starting gaytway API:", addr)
	err := http.ListenAndServe(addr, r)
	if err != nil {
		log.Fatal(err)
	}
}

func corsMiddleware(r chi.Router) {
	// Basic CORS
	// for more ideas, see: https://developer.github.com/v3/#cross-origin-resource-sharing
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	r.Use(cors.Handler)
}
