package main

import (
	messaged "bitbucket.org/PeterHueter/micromsg/cmd/messaged/service/proto"
	userd "bitbucket.org/PeterHueter/micromsg/cmd/userd/service/proto"
	"bitbucket.org/PeterHueter/micromsg/common"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"encoding/json"
	"github.com/go-chi/chi"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"net/http"
	"strconv"
)

func getUser(w http.ResponseWriter, r *http.Request) {
	// connection to userd service
	userdAddr := discovery.Userd()
	conn, err := grpc.Dial(userdAddr, grpc.WithInsecure())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()
	client := userd.NewUsersClient(conn)

	// make request and process errors
	userID, err := strconv.ParseUint(chi.URLParam(r, "userId"), 10, 32)
	if err != nil {
		http.Error(w, "UseID should be valid uint", http.StatusBadRequest)
		return
	}
	req := &userd.GetRequest{
		ID: []uint32{uint32(userID)},
	}
	user, err := client.Get(context.Background(), req)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	// json encode result
	data, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("getUser send response:", err)
	}
}

func currentUser(w http.ResponseWriter, r *http.Request) {
	// connection to userd service
	userdAddr := discovery.Userd()
	conn, err := grpc.Dial(userdAddr, grpc.WithInsecure())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()
	client := userd.NewUsersClient(conn)
	req := &rpc.Empty{}
	user, err := client.CurrentUser(r.Context(), req)
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	// json encode result
	data, err := json.Marshal(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("currentUser send response:", err)
	}
}

func getUserMessages(w http.ResponseWriter, r *http.Request) {
	// connection to userd service
	messagedAddr := discovery.Messaged()
	conn, err := grpc.Dial(messagedAddr, grpc.WithInsecure())
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer conn.Close()

	// rpc client
	client := messaged.NewMessagesClient(conn)
	userID, err := strconv.ParseUint(chi.URLParam(r, "userId"), 10, 32)
	if err != nil {
		http.Error(w, "UseID should be valid uint", http.StatusBadRequest)
		return
	}

	resp, err := client.ByUserID(context.Background(), &messaged.ByUserIDRequest{ID: uint32(userID)})
	if err != nil {
		msg, scode := common.HandleGrpcError(err)
		http.Error(w, msg, scode)
		return
	}

	// json encode result
	data, err := json.Marshal(resp.Messages)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if _, err = w.Write(data); err != nil {
		log.Println("getUserMessages send response:", err)
	}
}
