package middleware

import (
	tok "bitbucket.org/PeterHueter/micromsg/cmd/gatewayd/token"
	"bitbucket.org/PeterHueter/micromsg/common"
	"net/http"
)

// jwtAuth проверяет jwt токен из заголовка Authorization
func JwtAuth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenString, _, err := common.GetTokenFromRequest(r)
		if err != nil {
			writeError(w, http.StatusUnauthorized, err.Error())
			return
		}

		// проверить сделав запрос к сервису авторизации
		ok, err := tok.Valid(tokenString)
		if err != nil {
			msg, code := common.HandleGrpcError(err)
			writeError(w, code, msg)
			return
		}

		if !ok {
			writeError(w, http.StatusUnauthorized, "Unauthorized token")
		}

		next.ServeHTTP(w, r)
	})
}
