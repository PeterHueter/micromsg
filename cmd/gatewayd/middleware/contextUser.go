package middleware

import (
	"bitbucket.org/PeterHueter/micromsg/common"
	"net/http"
)

// Contextuser funciotn is set user from jwt to context
func ContextUser(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, _, err := common.GetTokenFromRequest(r)
		if token == "" || err != nil {
			next.ServeHTTP(w, r)
		}
		user, err := common.GetUserFromToken(token)
		if err != nil {
			writeError(w, http.StatusBadRequest, "user from token: "+err.Error())
			return
		}
		ctx := common.SetUserToContext(r.Context(), user)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
