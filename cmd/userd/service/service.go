package service

import (
	pb "bitbucket.org/PeterHueter/micromsg/cmd/userd/service/proto"
	"bitbucket.org/PeterHueter/micromsg/common"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

// Service is Implementation of UserService
type Service struct {
	Users *Users
}

// Get is used to get users profile by id
func (s *Service) Get(ctx context.Context, in *pb.GetRequest) (*pb.UsersResponse, error) {
	// user model to rpc answer
	response := &pb.UsersResponse{
		Users: s.Users.GetMany(in.GetID()),
	}
	return response, nil
}

// Get is used to get user profile by id
func (s *Service) GetByLogin(ctx context.Context, req *pb.LoginRequst) (*rpc.User, error) {
	user, ok := s.Users.GetByLogin(req.GetLogin())
	if !ok {
		return nil, grpc.Errorf(codes.NotFound, "User not found")
	}

	// user model to rpc answer
	resultUser := &rpc.User{
		ID:        user.ID,
		Login:     user.Login,
		FirstName: user.FirstName,
		LastName:  user.LastName,
	}
	return resultUser, nil
}

func (s *Service) CurrentUser(ctx context.Context, in *rpc.Empty) (*rpc.User, error) {
	u, err := common.GetUserFromContext(ctx)
	if err != nil {
		return nil, err
	}

	user, ok := s.Users.Get(u.ID)
	if !ok {
		return nil, grpc.Errorf(codes.NotFound, "User not found")
	}

	// user model to rpc answer
	resultUser := &rpc.User{
		ID:        user.ID,
		Login:     user.Login,
		FirstName: user.FirstName,
		LastName:  user.LastName,
	}
	return resultUser, nil
}
