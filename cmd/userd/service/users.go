package service

import (
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"encoding/json"
	"io"
	"io/ioutil"
	"sync"
)

type Users struct {
	users map[uint32]*rpc.User
	sync.RWMutex
}

type empty struct{}

func (u *Users) Load(r io.Reader) error {
	data, err := ioutil.ReadAll(r)
	if err != nil {
		return err
	}

	users := make([]*rpc.User, 20)
	err = json.Unmarshal(data, &users)

	u.Lock()
	u.users = make(map[uint32]*rpc.User, 20)
	for _, user := range users {
		u.users[user.ID] = user
	}
	u.Unlock()

	return err
}

func (u *Users) Get(id uint32) (*rpc.User, bool) {
	u.RLock()
	user, ok := u.users[id]
	u.RUnlock()

	return user, ok
}

func (u *Users) GetMany(ids []uint32) []*rpc.User {
	idMap := make(map[uint32]empty, len(ids))
	for _, v := range ids {
		idMap[v] = empty{}
	}

	users := make([]*rpc.User, 0, 10)
	u.RLock()
	for _, item := range u.users {
		if _, ok := idMap[item.ID]; ok {
			users = append(users, item)
		}
	}
	u.RUnlock()
	return users
}

func (u *Users) GetByLogin(login string) (user *rpc.User, ok bool) {
	u.RLock()
	for _, user = range u.users {
		if user.Login == login {
			ok = true
			break
		}
		user = nil
	}
	u.RUnlock()

	return user, ok
}

func (u *Users) Count() int {
	u.RLock()
	count := len(u.users)
	u.RUnlock()
	return count
}
