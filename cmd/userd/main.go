package main

import (
	"bitbucket.org/PeterHueter/micromsg/cmd/userd/service"
	pb "bitbucket.org/PeterHueter/micromsg/cmd/userd/service/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
	"time"
)

const userFile = "users.json"

const port = 44448

var serviceID string

func healthReport() {
	err := discovery.Register(serviceID, discovery.USERD, port)
	if err != nil {
		log.Fatal("health report: ", err)
	}

	for {
		err = discovery.Refresh(serviceID)
		if err != nil {
			log.Println("health report: ", err)
		}
		time.Sleep(time.Second * 10)
	}
}

func main() {
	serviceID = discovery.GenerateID()
	log.Println("Service ID:", serviceID)

	f, err := os.Open(userFile)
	if err != nil {
		log.Fatal(err)
	}

	users := &service.Users{}
	err = users.Load(f)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("LoadUser count:", users.Count())

	addr := discovery.Userd()
	sock, err := net.Listen("tcp", addr)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen socket:", addr)

	log.Println("Background health report")
	go healthReport()

	s := grpc.NewServer()
	pb.RegisterUsersServer(s, &service.Service{Users: users})
	log.Println("Runing gRPC service")
	err = s.Serve(sock)
	if err != nil {
		log.Fatal(err)
	}
}
