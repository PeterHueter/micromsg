package main

import (
	"encoding/json"
	"io/ioutil"
	"sync"
)

type jsonRepo struct {
	sync.RWMutex
	auths []*auth
}

func (repo *jsonRepo) Load(filename string) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	repo.Lock()
	err = json.Unmarshal(data, &repo.auths)
	repo.Unlock()
	return err
}

func (repo *jsonRepo) Count() int {
	repo.RLock()
	count := len(repo.auths)
	repo.RUnlock()
	return count
}

// Check is checking if user credentails match
func (repo *jsonRepo) Check(id uint32, password string) bool {
	ok := false
	repo.RLock()
	for _, auth := range repo.auths {
		if auth.UserID == id && auth.Password == password {
			ok = true
			break
		}
	}
	repo.RUnlock()
	return ok
}
