package main

import (
	"bitbucket.org/PeterHueter/micromsg/common"
	"bitbucket.org/PeterHueter/micromsg/rpc"
	"errors"
	jwt "github.com/dgrijalva/jwt-go"
	"time"
)

const tokenLifeSeconds = 15000

var secret = []byte("34h23kj4h32kj4h23kj4h")

type auth struct {
	UserID   uint32
	Password string
}

type authRepo interface {
	Count() int
	Check(id uint32, password string) bool
}

// create new JWT token
func newJWT(user *rpc.User) (string, error) {
	// Create the Claims
	claims := &common.UserClaims{
		User: user,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Unix() + tokenLifeSeconds,
			Issuer:    "auth",
			Subject:   user.GetLogin(),
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(secret)
}

// cehck if token is valid
func validJWT(token string) error {
	t, err := jwt.ParseWithClaims(token, &common.UserClaims{}, func(token *jwt.Token) (interface{}, error) {
		return secret, nil
	})

	if err != nil {
		return errors.New("parse token: " + err.Error())
	}

	if _, ok := t.Claims.(*common.UserClaims); ok && t.Valid {
		return nil
	}

	return errors.New("Invalid JWT structure")
}
