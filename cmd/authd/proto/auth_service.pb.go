// Code generated by protoc-gen-go. DO NOT EDIT.
// source: auth_service.proto

/*
Package proto is a generated protocol buffer package.

It is generated from these files:
	auth_service.proto

It has these top-level messages:
	Credentails
	Token
	CheckResponse
*/
package proto

import proto1 "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

import (
	context "golang.org/x/net/context"
	grpc "google.golang.org/grpc"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto1.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto1.ProtoPackageIsVersion2 // please upgrade the proto package

type Credentails struct {
	Login    string `protobuf:"bytes,1,opt,name=Login" json:"Login,omitempty"`
	Password string `protobuf:"bytes,2,opt,name=Password" json:"Password,omitempty"`
}

func (m *Credentails) Reset()                    { *m = Credentails{} }
func (m *Credentails) String() string            { return proto1.CompactTextString(m) }
func (*Credentails) ProtoMessage()               {}
func (*Credentails) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{0} }

func (m *Credentails) GetLogin() string {
	if m != nil {
		return m.Login
	}
	return ""
}

func (m *Credentails) GetPassword() string {
	if m != nil {
		return m.Password
	}
	return ""
}

type Token struct {
	Token string `protobuf:"bytes,1,opt,name=Token" json:"Token,omitempty"`
}

func (m *Token) Reset()                    { *m = Token{} }
func (m *Token) String() string            { return proto1.CompactTextString(m) }
func (*Token) ProtoMessage()               {}
func (*Token) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{1} }

func (m *Token) GetToken() string {
	if m != nil {
		return m.Token
	}
	return ""
}

type CheckResponse struct {
	Valid bool `protobuf:"varint,1,opt,name=valid" json:"valid,omitempty"`
}

func (m *CheckResponse) Reset()                    { *m = CheckResponse{} }
func (m *CheckResponse) String() string            { return proto1.CompactTextString(m) }
func (*CheckResponse) ProtoMessage()               {}
func (*CheckResponse) Descriptor() ([]byte, []int) { return fileDescriptor0, []int{2} }

func (m *CheckResponse) GetValid() bool {
	if m != nil {
		return m.Valid
	}
	return false
}

func init() {
	proto1.RegisterType((*Credentails)(nil), "Credentails")
	proto1.RegisterType((*Token)(nil), "Token")
	proto1.RegisterType((*CheckResponse)(nil), "CheckResponse")
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// Client API for Auth service

type AuthClient interface {
	Login(ctx context.Context, in *Credentails, opts ...grpc.CallOption) (*Token, error)
	Check(ctx context.Context, in *Token, opts ...grpc.CallOption) (*CheckResponse, error)
}

type authClient struct {
	cc *grpc.ClientConn
}

func NewAuthClient(cc *grpc.ClientConn) AuthClient {
	return &authClient{cc}
}

func (c *authClient) Login(ctx context.Context, in *Credentails, opts ...grpc.CallOption) (*Token, error) {
	out := new(Token)
	err := grpc.Invoke(ctx, "/Auth/Login", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *authClient) Check(ctx context.Context, in *Token, opts ...grpc.CallOption) (*CheckResponse, error) {
	out := new(CheckResponse)
	err := grpc.Invoke(ctx, "/Auth/Check", in, out, c.cc, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// Server API for Auth service

type AuthServer interface {
	Login(context.Context, *Credentails) (*Token, error)
	Check(context.Context, *Token) (*CheckResponse, error)
}

func RegisterAuthServer(s *grpc.Server, srv AuthServer) {
	s.RegisterService(&_Auth_serviceDesc, srv)
}

func _Auth_Login_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Credentails)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthServer).Login(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Auth/Login",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthServer).Login(ctx, req.(*Credentails))
	}
	return interceptor(ctx, in, info, handler)
}

func _Auth_Check_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Token)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AuthServer).Check(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/Auth/Check",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AuthServer).Check(ctx, req.(*Token))
	}
	return interceptor(ctx, in, info, handler)
}

var _Auth_serviceDesc = grpc.ServiceDesc{
	ServiceName: "Auth",
	HandlerType: (*AuthServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Login",
			Handler:    _Auth_Login_Handler,
		},
		{
			MethodName: "Check",
			Handler:    _Auth_Check_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "auth_service.proto",
}

func init() { proto1.RegisterFile("auth_service.proto", fileDescriptor0) }

var fileDescriptor0 = []byte{
	// 232 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x54, 0x90, 0xcd, 0x4b, 0x03, 0x31,
	0x10, 0xc5, 0x5b, 0x71, 0x4b, 0x1d, 0x3f, 0x0e, 0xc1, 0x43, 0x59, 0x10, 0x75, 0x41, 0xf0, 0x94,
	0x40, 0xf5, 0x2e, 0xda, 0x8b, 0x88, 0x87, 0xb2, 0x78, 0xf2, 0x22, 0xd9, 0x64, 0xd8, 0x0d, 0xdb,
	0xcd, 0x94, 0x7c, 0xd4, 0x7f, 0x5f, 0x92, 0xad, 0x62, 0x2f, 0x49, 0x7e, 0xc9, 0x7b, 0x2f, 0xc3,
	0x03, 0x26, 0x63, 0xe8, 0xbe, 0x3c, 0xba, 0x9d, 0x51, 0xc8, 0xb7, 0x8e, 0x02, 0x55, 0x4f, 0x70,
	0xba, 0x72, 0xa8, 0xd1, 0x06, 0x69, 0x36, 0x9e, 0x5d, 0x42, 0xf1, 0x4e, 0xad, 0xb1, 0x8b, 0xe9,
	0xcd, 0xf4, 0xfe, 0xa4, 0x1e, 0x81, 0x95, 0x30, 0x5f, 0x4b, 0xef, 0xbf, 0xc9, 0xe9, 0xc5, 0x51,
	0x7e, 0xf8, 0xe3, 0xea, 0x0a, 0x8a, 0x0f, 0xea, 0xd1, 0x26, 0x6b, 0x3e, 0xfc, 0x5a, 0x33, 0x54,
	0x77, 0x70, 0xbe, 0xea, 0x50, 0xf5, 0x35, 0xfa, 0x2d, 0x59, 0x8f, 0x49, 0xb6, 0x93, 0x1b, 0xa3,
	0xb3, 0x6c, 0x5e, 0x8f, 0xb0, 0x7c, 0x83, 0xe3, 0xe7, 0x18, 0x3a, 0x76, 0xbd, 0xff, 0x9f, 0x9d,
	0xf1, 0x7f, 0x63, 0x95, 0x33, 0x3e, 0xa6, 0x4d, 0xd8, 0x2d, 0x14, 0x39, 0x8f, 0xed, 0xaf, 0xca,
	0x0b, 0x7e, 0x90, 0x5f, 0x4d, 0x5e, 0x1e, 0x3f, 0x97, 0x8d, 0x09, 0x4d, 0x54, 0x3d, 0x06, 0x4e,
	0xae, 0x15, 0x6b, 0x0c, 0xe8, 0x5e, 0x63, 0x5a, 0xc5, 0x60, 0x94, 0xa3, 0xc1, 0xb7, 0x42, 0x0d,
	0x5a, 0xa4, 0x3e, 0xb4, 0xc8, 0x45, 0x34, 0xb3, 0xbc, 0x3d, 0xfc, 0x04, 0x00, 0x00, 0xff, 0xff,
	0xc3, 0xb2, 0x51, 0x24, 0x25, 0x01, 0x00, 0x00,
}
