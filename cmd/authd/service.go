package main

import (
	pb "bitbucket.org/PeterHueter/micromsg/cmd/authd/proto"
	userd "bitbucket.org/PeterHueter/micromsg/cmd/userd/service/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
)

type service struct {
	Repo authRepo
}

func (svc *service) Login(ctx context.Context, in *pb.Credentails) (*pb.Token, error) {
	// получить данные пользователя
	userdAddr := discovery.Userd()
	conn, err := grpc.Dial(userdAddr, grpc.WithInsecure())
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "Can't connect to user service: "+err.Error())
	}
	defer conn.Close()

	client := userd.NewUsersClient(conn)
	user, err := client.GetByLogin(context.Background(), &userd.LoginRequst{Login: in.GetLogin()})
	if err != nil {
		return nil, err
	}

	// аутефицировать пользователя
	ok := svc.Repo.Check(user.ID, in.Password)
	if !ok {
		return nil, grpc.Errorf(codes.Unauthenticated, "Invalid credentails")
	}

	token, err := newJWT(user)
	if err != nil {
		return nil, grpc.Errorf(codes.Internal, "Token error: %v", err)
	}
	return &pb.Token{Token: token}, nil
}
func (svc *service) Check(ctx context.Context, in *pb.Token) (*pb.CheckResponse, error) {
	err := validJWT(in.GetToken())
	if err != nil {
		return nil, grpc.Errorf(codes.Unauthenticated, "Invalid credetails: %v", err)
	}
	return &pb.CheckResponse{Valid: true}, nil
}
