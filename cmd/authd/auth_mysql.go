package main

import (
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
	"log"
)

type mysqlRepo struct {
	db *sqlx.DB
}

func (repo *mysqlRepo) Count() int {
	var count int
	err := repo.db.Get(&count, "SELECT count(user_id) FROM auth")
	if err != nil {
		log.Println("count users:", err)
	}
	return count
}

func (repo *mysqlRepo) Check(id uint32, password string) bool {
	ids := []uint{}
	err := repo.db.Select(&ids, "SELECT user_id FROM auth WHERE password = ?", password)
	if err != nil {
		log.Println("count users:", err)
	}
	return len(ids) > 0
}

func newMysqlRepo(dsn string) (*mysqlRepo, error) {
	db, err := sqlx.Connect("mysql", dsn)
	if err != nil {
		return nil, err
	}
	return &mysqlRepo{db: db}, nil
}
