package main

import (
	pb "bitbucket.org/PeterHueter/micromsg/cmd/authd/proto"
	"bitbucket.org/PeterHueter/micromsg/discovery"
	"google.golang.org/grpc"
	"log"
	"net"
	"time"
)

const fileName = "auth.json"
const port = 51000

var serviceID string

func healthReport() {
	err := discovery.Register(serviceID, discovery.AUTHD, port)
	if err != nil {
		log.Fatal("health report: ", err)
	}

	for {
		err = discovery.Refresh(serviceID)
		if err != nil {
			log.Println("health report: ", err)
		}
		time.Sleep(time.Second * 10)
	}
}

func main() {
	serviceID = discovery.GenerateID()
	log.Println("Service ID:", serviceID)

	/*
		repo := &authRepo{}
		err := repo.Load(fileName)
		if err != nil {
			log.Fatal(err)
		}
	*/

	repo, err := newMysqlRepo("micromsg:micromsg@(localhost)/micromsg")
	if err != nil {
		log.Fatalf("mysql user repo: %v", err)
	}
	log.Println("Loaded", repo.Count(), "auths")

	authdAddr := discovery.Authd()
	sock, err := net.Listen("tcp", authdAddr)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("Listen socket:", authdAddr)

	log.Println("Background health report")
	go healthReport()

	s := grpc.NewServer()
	pb.RegisterAuthServer(s, &service{Repo: repo})
	log.Println("Runing gRPC service")
	err = s.Serve(sock)
	if err != nil {
		log.Fatal(err)
	}
}
