package main

import (
	pb "bitbucket.org/PeterHueter/micromsg/cmd/userd/service/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"log"
)

func main() {
	testUserd()
}

func testUserd() {
	const addr = ":44448"
	const userId = 2

	conn, err := grpc.Dial(addr, grpc.WithInsecure())
	if err != nil {
		log.Fatal("Can't connect to userd service:", err)
	}
	defer conn.Close()

	client := pb.NewUsersClient(conn)
	req := &pb.GetRequest{
		ID: userId,
	}
	user, err := client.Get(context.Background(), req)
	s, ok := status.FromError(err)
	if err != nil {
		var msg string
		if ok {
			msg = "rpc error: " + s.Message()
		} else {
			msg = "system error: " + err.Error()
		}

		log.Fatal(msg)
	}
	log.Println("User found:",
		"\n\tID:", user.ID,
		"\n\tLogin:", user.Login,
		"\n\tFirstName:", user.FirstName,
		"\n\tLastName:", user.LastName)
}
