#!env bash

pwd=`pwd`

# list of services for building images
services="authd gatewayd messaged tagd userd"

for service in $services
do
  cd "$pwd/cmd/$service"

  # build with disabled cgo
  CGO_ENABLED=0 GOOS=linux go build -ldflags="-s -w"

  # compress
  #upx --brute $service

  # create docker image
  docker build . -t $service
done