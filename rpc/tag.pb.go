// Code generated by protoc-gen-go. DO NOT EDIT.
// source: tag.proto

package rpc

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type Tag struct {
	Text string `protobuf:"bytes,1,opt,name=Text" json:"Text,omitempty"`
}

func (m *Tag) Reset()                    { *m = Tag{} }
func (m *Tag) String() string            { return proto.CompactTextString(m) }
func (*Tag) ProtoMessage()               {}
func (*Tag) Descriptor() ([]byte, []int) { return fileDescriptor1, []int{0} }

func (m *Tag) GetText() string {
	if m != nil {
		return m.Text
	}
	return ""
}

func init() {
	proto.RegisterType((*Tag)(nil), "Tag")
}

func init() { proto.RegisterFile("tag.proto", fileDescriptor1) }

var fileDescriptor1 = []byte{
	// 104 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x2c, 0x49, 0x4c, 0xd7,
	0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x57, 0x92, 0xe4, 0x62, 0x0e, 0x49, 0x4c, 0x17, 0x12, 0xe2, 0x62,
	0x09, 0x49, 0xad, 0x28, 0x91, 0x60, 0x54, 0x60, 0xd4, 0xe0, 0x0c, 0x02, 0xb3, 0x9d, 0x34, 0xa2,
	0xd4, 0x92, 0x32, 0x4b, 0x92, 0x4a, 0x93, 0xb3, 0x53, 0x4b, 0xf4, 0xf2, 0x8b, 0xd2, 0xf5, 0x03,
	0x52, 0x4b, 0x52, 0x8b, 0x3c, 0x4a, 0x41, 0xa4, 0x7e, 0x6e, 0x66, 0x72, 0x51, 0x7e, 0x6e, 0x71,
	0xba, 0x7e, 0x51, 0x41, 0x72, 0x12, 0x1b, 0xd8, 0x2c, 0x63, 0x40, 0x00, 0x00, 0x00, 0xff, 0xff,
	0xf3, 0x1a, 0x6d, 0x83, 0x58, 0x00, 0x00, 0x00,
}
