| Метод | Путь                       | Описание    |
|-------|----------------------------|-------------|
| GET   |`/users/me`                 | Текущий автризированный пользователь |
| GET   |`/users/{userId}`           | Профиль пользователя |
| GET   |`/users/{userId}/messages`  | Записи пользователя |
| GET   |`/messages/{msgId}/comments`| Кометарии к записи |
| POST  |`/messages/{msgId}/comments`| Оставить кометарий к записи |
| GET   |`/messages`                 | Записи текущего пользователя |
| POST  |`/messages`                 | Добавить запись пользователю |
| POST  |`/auth/login`               | Получение JWT |
| GET   |`/auth/check`               | Проверка JWT |
